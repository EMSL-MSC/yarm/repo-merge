#!/bin/bash


for r in repos/*.merge
do
	name=`basename ${r/.merge//}`
	case $1 in
	deploy)
		md1=`kubectl get configmap ${name}-repo-merge -o jsonpath="{.data.repo-config}"|md5sum|cut -c1-32`
		md2=`md5sum $r | cut -c1-32`
		if [[ $md1 != $md2 || ${name: -5} == daily ]]
		then
			echo ============ Deploying $r ================
			if helm status $name
			then
				helm upgrade $name --set configdata=$(base64 -w0 -i $r) --wait --timeout 1800 repo-merge &
			else
				helm install -n $name --set configdata=$(base64 -w0 -i $r) --wait --timeout 1800 repo-merge &
			fi
            sleep 1 # give helm a second to output
		fi
		BUILDFAILED=false
		for i in $(jobs -p)
		do
			if ! wait $i
			then
				BUILDFAILED=true
			fi
		done
		if $BUILDFAILED
		then
			set +x
			echo "********************"
			echo "********************"
			echo "*** BUILD FAILED ***"
			echo "********************"
			echo "********************"
			exit 1
		fi
		;;
	destroy)
		echo ============ Destroying $r ================
		helm del --purge $name  &
		;;
	*)
		echo Confused about life.
		;;
	esac
done

