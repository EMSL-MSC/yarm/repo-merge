#!/bin/bash -xe

# we should recieve an config file in /merge/config, 
# which has a set repos and snapshots that we should use,
# for example:
# centos7-base 20180622.1
# ceph-luminous 20180622.1
# ceph-luminous-noarch 20180622.1
# we will sym link these into /merge
# then run createrepo in that directory

extra=
comps=

while read name snap;
do
    if [ ${snap} == daily ]
    then
    	ln -s /repo/${name} /merge/${name} || true
    else
    	ln -s /repo/.zfs/snapshot/${snap}/${name} /merge/${name} || true
    fi
    for c in $( find /merge/${name}/ -type f -name '*comps*.xml' )
    do
      comps="$comps --load $c"
    done

done < /merge/config.txt

cd /merge

if [ ! -z "$comps" ]
then
	yum-groups-manager $comps --save comps.xml -n base
    extra="-g comps.xml"
fi


createrepo $extra --workers 32 .
echo 
echo ================ Repo Created ===================
echo
