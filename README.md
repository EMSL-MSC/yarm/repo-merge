Repository Merger
=================

This repository contains the repositories and tools needed to deploy them using a kubernetes helm chart.

Componets
---------

* Dockerfile - Dockerfile to add a merging script into a docker image that has nginx and createrepo (https://github.com/karcaw/nginx-createrepo) which is built externally to keep this build quick. The ```merge_repos.sh``` does this merging
* repo-merge helm chart - This helm chart uses the Docker image to deploy merged repositories into the ```https://yarm.emsl.pnl.gov/repo/<repo>/``` URL space.
* repos directory - This contains the merge files that specify what the merged repositories consist of.  Each ```<file>.merge``` is presented as a url using the helm chart during the CI process.

New Repositories
----------------

To edit an exisiting repository, or create a new one, edit files in the repos directory.  Files must be named ```<file>.merge``` to be picked up, and then the Gitlab CI process will create or update the repository.

Repositories should show up here:
```https://yarm.emsl.pnl.gov/repo/<file>/```