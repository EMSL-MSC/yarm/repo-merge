FROM karcaw/nginx-createrepo:latest

MAINTAINER evan.felix@pnnl.gov

COPY merge_repos.sh /
COPY default.conf /etc/nginx/conf.d/
RUN mkdir /merge
RUN touch /merge/config.txt

CMD /merge_repos.sh
